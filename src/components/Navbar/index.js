import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { backgroundNavbar } from './style';
import { Navbar } from 'react-bootstrap';

const NavBar = () => {
  return (
    <Navbar collapseOnSelect fixed='top' className={backgroundNavbar}>
      
      <Navbar.Brand href="#" className="pojokKiri">
      Toko Madu
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        
      </Navbar.Collapse>
    </Navbar>
  );
};
export default NavBar;
