import { css } from '@emotion/css';

export const backgroundNavbar = css`
  background-color: #ED9028;
  font-size: 18px;
  font-family: "Source Sans Pro", sans-serif;
  max-width: 100%;
  behavior: smooth;
  scroll-behavior: smooth;

  .nav-link {
    align-self: center;
  }
  .pojokKiri{
    font-family: 'Poppins', sans-serif;
    font-size:calc(10px + 1vw);
    color:#664F4C;
  }

  .nav-item {
    align-self: center;
  }

  .nav-link {
    padding-left: 2rem !important;
  }
`;