import styled from '@emotion/styled';

export const Content = styled.div`
    @media screen {

        .TittleHome{
            font-family: 'Otomanopee One', sans-serif;
            text-align: center;
            font-size: calc(40px + 4vw);
            align-content: center;
            padding-top: 10px;
            text-shadow: 2px 2px #D2D4DD;
            max-width: 100%;
        }

        .harga{
            font-size : calc(15px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        .row{
            max-width: 100%;
        }

        .col{
            max-width: 100%;
        }

        .gambarHome{
            width: 400px;
        }

        .gambar{
            width: 1px;
        }

        .Gradient{
            height: 100px;
            background-color: black;
            max-width: 100%;
        }


        .tabkum {
            font-size : calc(10px + 1vw);
        }

        .container-list {
            margin-top:50px;
            margin-bottom:50px;
            max-width: 1000px;
            align-content: center;
        }
        .rowatas {
            margin-bottom: 50px;
            justify-content: space-between;
        }

        .sambutan{
            text-align:center;
            margin:auto;
            max-width:1000px;
            font-size : calc(15px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        .akhiran{
            text-align:center;
            margin:auto;
            max-width:1000px;
            font-size : calc(12px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        a{
            color : #656565 ;
            font-weight: 500;
        }
        
        a:hover{
            color: #001457;
        }
        
        .nav-link.active{
            color: #black;
            background-color: #F8CF2C;
        }

        background-color : #EDF2F6;

        max-width: 100%;
        min-height: 100vh;

    @media screen and (max-width: 1300px) {
        .tabs-filter {
            margin-left: 2%;
            margin-right: 2%;
        }
    } 
            
        `;
