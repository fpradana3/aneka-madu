import React, { useEffect } from 'react';
import { Button, Modal, Col, Row } from 'react-bootstrap';
import { Content } from './style';

const ModalKhasiat = ({ show, onHide, gambar, harga, listKhasiat, merek, opsi1, opsi2, hrg1, hrg2, handleHargaChange, handleProdukDibeliChange }) => {
  const infoKhasiat = [];

  useEffect(() => {
    for (var i of listKhasiat) {
      infoKhasiat.push(i);
  }
    console.log("di sini munculnya")
    console.log(listKhasiat)
    console.log(infoKhasiat)
  },[]);
  

  return (
  <Content>
    <Modal
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton onClick={onHide} style={{backgroundColor: "#F8CF2C"}}>
        <Modal.Title style={{color:"#664F4C"}}id="contained-modal-title-vcenter">
          {merek}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col>
              <img src={gambar} alt="gambar" style={{ width: '15rem', height: '15rem', padding: '10px', marginLeft:'5vw' }} />
            <div className="harga" style={{ textAlign:'center', fontSize: 'calc(5px + 1vw)', fontFamily: "'Open Sans', sans-serif"}}>{harga}</div>
          </Col>
          <Col style={{fontSize: 'calc(5px + 1vw)', fontFamily: "'Open Sans', sans-serif"}}>
            <div className="deskripsi">Deskripsi:</div>
            <p>{' '}</p>
            <div className="khasiat">{listKhasiat.map((
                    (data, index) => {
                      console.log(data);
                      console.log("masuk dong")
                      return (
                      <div className="listKhasiat">{index + 1} {data.khasiat} </div>
                      
                    )}
                  ))}</div>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer style={{backgroundColor: "#D2D4DD"}}>
      <Button variant="success" onClick={() => { onHide(); handleHargaChange(hrg1); handleProdukDibeliChange(merek,opsi1); }}>Beli {opsi1}</Button>
      {opsi2 !== "x"
          && (
            <Button variant="success" onClick={() => { onHide(); handleHargaChange(hrg2); handleProdukDibeliChange(merek,opsi2); }}>Beli {opsi2}</Button>
          )}
        <Button className="btnCancel" variant="danger" onClick={onHide}>Kembali</Button>
      </Modal.Footer>
    </Modal>
  </Content>
)};

export default ModalKhasiat;
