import React from 'react';
import { Button, Modal, Col, Row } from 'react-bootstrap';
import { Content } from './style';
import wa from '../assets/whatsapp2.gif';

const ModalCheckout = ({ show, onHide, listBelanja }) => {  

  return (
  <Content>
    <Modal
      show={show}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton onClick={onHide} style={{backgroundColor: "#F8CF2C"}}>
        <Modal.Title style={{color:"#664F4C", fontSize:"20px"}}id="contained-modal-title-vcenter">
          Chekcout Belanjaanmu
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col xs={8}>
          <div className="deskripsi">Halo, Saya ingin membeli:</div>
            <p>{' '}</p>
            <div className="khasiat">{listBelanja.map((
                    (data, index) => 
                      (
                      <div className="listKhasiat">{index + 1} {data.nama} </div>
                      
                    )
                  ))}</div>
            <div className="deskripsi" style={{marginTop:"10px"}}>Apakah Produk masih tersedia?</div>
          </Col>
          <Col style={{fontSize: 'calc(1px + 1vw)', fontFamily: "'Open Sans', sans-serif"}}>
              <div style={{marginBottom:"30px"}}>Salin Pesan di samping dan kirim via whatsapp ke:</div>
              <Row>
              <img src={wa} alt="gambar" style={{ width: '2rem', height: '2rem', }} />
              <div style={{marginTop:"5px", marginLeft:"5px"}}>085697977212</div>
              </Row>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer style={{backgroundColor: "#D2D4DD", alignContent:"center"}}>
        <Button className="btnCancel" variant="danger" style={{alignContent:"center"}} onClick={onHide}>Kembali</Button>
      </Modal.Footer>
    </Modal>
  </Content>
)};

export default ModalCheckout;
