import React, {useState} from 'react';
import { Card, Col, Button} from 'react-bootstrap';
import ModalKhasiat from '../ModalKhasiat';

const CardsMadu = ({
  gambar, merek, harga, fullinfo, handleHargaChange, handleProdukDibeliChange
}) => {
  const [modalShow, setModalShow] = useState(false);

  return (
  <>
    <Col sm="6" md="4" lg="3" className="mb-3">
      <Card style={{
        width: '13.6rem', backgroundColor: 'white', borderColor: '#F8EA8C', borderWidth: 'thick',
      }}
      >
        <Card.Img style={{ width: '13rem', height: '15rem', padding: '10px' }} variant="top" src={gambar} />
        <Card.Body>
          <Card.Title style={{ fontSize: '23px' }}>{merek}</Card.Title>
          <Card.Text>
            {harga}
          </Card.Text>
          <div className="w-100 text-center">
            <Button style={{ backgroundColor: '#ED9028', borderColor:"#664F4C", borderWidth:"3px"}} onClick={() => setModalShow(true)}>Lihat Detail</Button>
          </div>
        </Card.Body>
      </Card>
    </Col>
    {modalShow === true
          && (
    <ModalKhasiat
          show={modalShow}
          onHide={() => setModalShow(false)}
          gambar={fullinfo.img}
          handleHargaChange={handleHargaChange} 
          harga={fullinfo.harga_full} 
          listKhasiat={fullinfo.listKhasiat} 
          merek={merek}
          opsi1={fullinfo.opsi1.ukuran}
          hrg1={fullinfo.opsi1.harga}
          opsi2={fullinfo.opsi2.ukuran}
          hrg2={fullinfo.opsi2.harga}
          handleProdukDibeliChange={handleProdukDibeliChange}
        />)}
  </>
  )
};

export default CardsMadu;
