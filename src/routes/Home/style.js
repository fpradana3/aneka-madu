import styled from '@emotion/styled';
import bghd from './assets/gambar1.png';

export const Content = styled.div`
    @media screen {

        scroll-behavior: smooth;
        behavior: smooth;
        scroll-behavior: smooth;

        .TittleHome{
            font-family: 'Otomanopee One', sans-serif;
            text-align: center;
            font-size: calc(40px + 4vw);
            align-content: center;
            padding-top: 10px;
            text-shadow: 2px 2px #D2D4DD;
            max-width: 100%;
        }

        .harga{
            font-size : calc(15px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        .row{
            max-width: 100%;
        }

        .col{
            max-width: 100%;
        }

        .gambarHome{
            width: 400px;
        }

        .gambar{
            width: 1px;
        }

        .Header{
            padding-top: calc(70px + 6vw);
            padding-bottom: 10vw;
            background-color:#edf2f6;
            background-image: url(${bghd});
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: bottom;
            max-width: 100%;
            margin-bottom:30px;
        }

        .Gradient{
            height: 100px;
            background-color: black;
            max-width: 100%;
        }


        .tabkum {
            font-size : calc(10px + 1vw);
        }

        .container-list {
            margin-top:50px;
            margin-bottom:50px;
            max-width: 1000px;
            align-content: center;
        }
        .rowatas {
            margin-bottom: 50px;
            justify-content: space-between;
        }

        .sambutan{
            text-align:center;
            margin:auto;
            max-width:1000px;
            font-size : calc(15px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        .txtFooter{
            text-align:center;
            margin:auto;
            max-width:1000px;
            font-size : calc(10px + 1vw);
            font-family: 'Poppins', sans-serif;
            color:white;
        }

        .nomor{
            text-align:center;
            margin-top:10px;
            font-size : calc(10px + 1vw);
            font-family: 'Poppins', sans-serif;
            color:white;
        }

        .listBelanja{
            text-align:left;
            max-width:1000px;
            font-size : calc(12px + 1vw);
            font-family: 'Poppins', sans-serif;
            padding-bottom:50px;
            margin-top:20px;
            margin-left:50px;
        }

        .hargaBelanja{
            text-align:left;
            font-size : calc(12px + 1vw);
            font-family: 'Poppins', sans-serif;
            padding-bottom:30px;
            margin-left:50px;
        }

        .isiBelanjaan{
            font-size : calc(2px + 1vw);
            font-family: 'Poppins', sans-serif;
        }

        .tombol{
            padding-bottom:80px;
        }

        .totalHarga{
            font-size : calc(5px + 1vw);
            font-family: 'Poppins', sans-serif;
            color: green;
            margin-top:20px;
            margin-right:70px;
            text-align:center;
        }

        .footer{
            padding-top:30px;
            padding-bottom:30px;
            background-color:#664F4C;
        }

        a{
            color : #656565 ;
            font-weight: 500;
        }
        
        a:hover{
            color: #001457;
        }
        
        .nav-link.active{
            color: #black;
            background-color: #F8CF2C;
        }

        background-color : #EDF2F6;

        max-width: 100%;
        min-height: 100vh;
        scroll-behavior: smooth;

    @media screen and (max-width: 1300px) {
        .tabs-filter {
            margin-left: 2%;
            margin-right: 2%;
        }
        scroll-behavior: smooth;
    } 
            
        `;
