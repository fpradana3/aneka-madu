import React, { useState, useEffect } from 'react';
import Navbar from '../../components/Navbar';
import { Content } from './style';
import { Col, Row, Tabs, Tab, Container, Card, Button } from 'react-bootstrap';
import kelengkeng from './assets/madu_kelengkeng.jpg';
import durian from './assets/madu_durian.jpg';
import kopi from './assets/madu_kopi.jpg';
import kaliandra from './assets/madu_kaliandra.jpg';
import mangga from './assets/madu_mangga.jpg';
import rambutan from './assets/madu_rambutan.jpg';
import mahoni from './assets/madu_mahoni.jpg';
import sonokeling from './assets/madu_sonokeling.jpg';
import kapuk_randu from './assets/madu_kapuk_randu.jpg';
import multiflora from './assets/madu_multiflora.jpg';
import organik from './assets/madu_organik.jpg';
import hutan from './assets/madu_hidayah_hutan.jpg';
import ternak from './assets/madu_hidayah_ternak.jpg';
import trigona from './assets/madu_trigona.jpg';
import royal_jelly from './assets/madu_royal_jelly.jpg';
import pasutri from './assets/madu_pasutri.jpg';
import hamil from './assets/madu_hamil_menyusui.jpg';
import anak from './assets/madu_anak.jpg';
import propolis from './assets/madu_propolis.jpg';
import pelangsing from './assets/madu_pelangsing.jpg';
import kulit_manggis from './assets/madu_kulit_manggis.jpg';
import herbal_plus from './assets/madu_herbal_plus.jpg';
import msuper from './assets/madu_super.jpg';
import tugu_sakti from './assets/madu_tugu_sakti.jpg';
import smt from './assets/sabun_madu.jpg';
import sms from './assets/sabun_sereh.jpg';
import smp from './assets/sabun_pala.jpg';
import smpro from './assets/sabun_propolis.jpg';
import smc from './assets/sabun_madu_cair.jpg';
import sm from './assets/shampoo_madu.jpg';
import mm from './assets/masker_madu.jpg';
import cart from './assets/cart.png'
import CardsMadu from './Card';
import ModalCheckout from './ModalCheckout';
import '../../index.css';
import wa from './assets/whatsapp2.gif'



const Home = () => {
  const [state, setState] = useState('Madu Murni');
  const [infoProduk, setInfoProduk] = useState([]);
  const [produkDibeli, setProdukDibeli] = useState([]);
  const [harga, setHarga] = useState(0);
  const [modalShow, setModalShow] = useState(false);

  const handleHargaChange = (e) => {
    setHarga(harga + e);
  };

  const resetData = () => {
    setHarga(0);
    setProdukDibeli([]);
  }

  const handleProdukDibeliChange = (e, i) => {
    var namaProduk = {nama: `${e} ${i}`}
    setProdukDibeli([...produkDibeli, namaProduk])
    console.log(produkDibeli);
  };

  

  useEffect(() => {
    setInfoProduk([]);
    switch (state) {
      case 'Madu Formula':
        setInfoProduk([{nama: 'Madu Royal Jelly', harga:'400 gr Rp 110.000', img: royal_jelly, id:'1',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Meningkatkan produksi hormon"},
                                          {khasiat:"Menyuburkan peranakan"},
                                          {khasiat:"Memperbaiki organ tubuh yang rusak"},
                                          {khasiat:"Memperkuat fungsi otak"},
                                          {khasiat:"Meninghilangkan rasa letih berkepanjangan"},
                                          {khasiat:"Mempercepat penyembuhan pasca operasi"},
                                          {khasiat:"Baik untuk masker wajah"},
                                          {khasiat:"Terdiri dari madu & royal jelly"}]},
                        {nama: 'Madu Pasutri', harga:'400 gr Rp 110.000', img: pasutri, id:'2',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Meningkatkan kinerja seksual pria"},
                                          {khasiat:"Meningkatkan vitalitas dan kebugaran"},
                                          {khasiat:"Memperkuat sumsum tulang"},
                                          {khasiat:"Baik diminum pria dan wanita"},
                                          {khasiat:"Terdiri dari madu & ekstrak pasak bumi"}]},
                        {nama: 'Madu Ibu Hamil/Menyusui', harga:'400 gr Rp 110.000', img: hamil, id:'3',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Memperbanyak ASI"},
                                          {khasiat:"Sebagai sumber nutrisi"},
                                          {khasiat:"Mengurangi keriput setelah melahirkan"},
                                          {khasiat:"Mencegah dan mengobati infeksi"},
                                          {khasiat:"Terdiri dari madu & ekstrak daun katuk"}]},
                        {nama: 'Madu Anak', harga:'400 gr Rp 110.000',img:anak, id:'4',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Merangsang kecerdasan otak"},
                                          {khasiat:"Meningkatkan nafsu makan"},
                                          {khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Mengobati sariawan dan luka bakar"},
                                          {khasiat:"Mengobati demam dan panas dalam"},
                                          {khasiat:"Sebagai suplemen makanan"},
                                          {khasiat:"Terdiri dari madu & royal jelly"}]},
                        {nama: 'Madu Propolis', harga:'400 gr Rp 135.000',img:propolis, id:'5',
                            opsi1:{ukuran:"400 gr", harga:135000},
                            opsi2:{ukuran:"1 kg", harga:310000},
                            harga_full:"400 gr Rp 135.000 // 1 kg Rp 310.000", 
                            listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Membantu membuang racun dari tubuh"},
                                          {khasiat:"Mencegah alergi"},
                                          {khasiat:"Sebagai antibiotik alami"},
                                          {khasiat:"Sebagai antioksidan"},
                                          {khasiat:"Mengandung nutrisi yang tinggi"},
                                          {khasiat:"Sangat baik bagi penderita penyakit kronis"},
                                          {khasiat:"Terdiri dari madu, propolis, royal jelly, pollen ( serbuk sari )"}]},
                        {nama: 'Madu Pelangsing', harga:'400 gr Rp 110.000',img:pelangsing, id:'6',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Dapat dikonsumsi sebagai pengganti nutrisi tubuh"},
                                          {khasiat:"Membantu mengurangi lemak tubuh"},
                                          {khasiat:"Menurunkan berat badan"},
                                          {khasiat:"Memperkuat daya tahan tubuh"},
                                          {khasiat:"Terdiri dari madu & ekstrak jati Belanda"}]},
                        {nama: 'Madu Kulit Manggis', harga:'400 gr Rp 110.000',img:kulit_manggis, id:'7',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Mengatasi penyakit degeneratif"},
                                          {khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Menyehatkan jantung"},
                                          {khasiat:"Memperbaiki sel tubuh yang rusak"},
                                          {khasiat:"Membantu menghentikan diare"},
                                          {khasiat:"Mengatasi penyakit kulit kemerah-merahan"},
                                          {khasiat:"Mengandung antioksidan"}]},
                        {nama: 'Madu Herbal Plus', harga:'400 gr Rp 110.000',img:herbal_plus, id:'8',
                        opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Menstabilkan tekanan darah"},
                                          {khasiat:"Meningkatkan kecerdasan otak"},
                                          {khasiat:"Meningkatkan vitalitas seksual"},
                                          {khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Membantu penyembuhan panas dalam, radang tenggorokan, dan bau mulut"},
                                          {khasiat:"Mengobati reumatik dan asam urat"},
                                          {khasiat:"Menghilangkan rasa letih"},
                                          {khasiat:"Dapat dikonsumsi penderita diabetes"},
                                          {khasiat:"Terdiri dari madu, habbatussauda, royal jelly, pollen ( serbuk sari )"}]},
                        {nama: 'Madu Super', harga:'400 gr Rp 110.000',img:msuper, id:'9',
                        opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                            harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                            listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                          {khasiat:"Menguatkan fungsi otak"},
                                          {khasiat:"Menguatkan fungsi jantung"},
                                          {khasiat:"Memperbaiki sel tubuh yang rusak"},
                                          {khasiat:"Menghilangkan rasa letih"},
                                          {khasiat:"Mengobati sakit pinggang"},
                                          {khasiat:"Dapat dikonsumsi penderita diabetes"},
                                          {khasiat:"Bisa untuk masker wajah"},
                                          {khasiat:"Terdiri dari madu, royal jelly, pollen ( serbuk sari )"}]},
                        {nama: 'Madu Tugu Sakti', harga:'250 g Rp 160.000',img:tugu_sakti, id:'10',
                        opsi1:{ukuran:"400 gr", harga:160000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"250 g Rp 160.000", 
                            listKhasiat:[{khasiat:"Meningkatkan stamina"},
                                          {khasiat:"Meningkatkan kinerja seksual pria"}]}]);
        break;
      case 'Madu Kosmetik':
        setInfoProduk([{nama: 'Sabun Madu Transparan', harga:'4 batang Rp 110.000',img:smt, id:'1',
                            opsi1:{ukuran:"4 batang", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"4 batang Rp 110.000", 
                            listKhasiat:[{khasiat:"Menjaga kelembutan kulit"},
                                          {khasiat:"Mencegah dan mengobati penyakit kulit"},
                                          {khasiat:"Mengurangi pertumbuhan bakteri pada kulit"},
                                          {khasiat:"Mengurangi bau badan"}]},
                      {nama: 'Sabun Madu Sereh', harga:'4 batang Rp 110.000',img:sms, id:'2',
                      opsi1:{ukuran:"4 batang", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"4 batang Rp 110.000", 
                            listKhasiat:[{khasiat:"Menjaga kelebaban dan kelembutan kulit"},
                                          {khasiat:"Mencerahkan kulit"},
                                          {khasiat:"Mencegah gigitan nyamuk / serangga"},
                                          {khasiat:"Mengatasi berbagai masalah kulit"},
                                          {khasiat:"Mengurangi bau badan"},
                                          {khasiat:"Mengandung minyak sereh"}]},
                      {nama: 'Sabun Madu Pala', harga:'4 batang Rp 110.000',img:smp, id:'3',
                      opsi1:{ukuran:"4 batang", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"4 batang Rp 110.000", 
                            listKhasiat:[{khasiat:"Menjaga kelebaban dan kelembutan kulit"},
                                          {khasiat:"Mencerahkan kulit"},
                                          {khasiat:"Mengurangi noda flek hitam dan jerawat"},
                                          {khasiat:"Cocok untuk wajah"},
                                          {khasiat:"Mengatasi berbagai masalah kulit"},
                                          {khasiat:"Mengurangi bau badan"},
                                          {khasiat:"Mengandung ekstrak buah pala"}]},
                      {nama: 'Sabun Madu Propolis', harga:'3 batang Rp 100.000',img:smpro, id:'4',
                      opsi1:{ukuran:"3 batang", harga:100000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"3 batang Rp 100.000", 
                            listKhasiat:[{khasiat:"Melembutkan kulit"},
                                          {khasiat:"Mengurangi keputihan pada wanita"},
                                          {khasiat:"Membantu membunuh bakteri pada kulit"},
                                          {khasiat:"Mengurangi bau badan"},
                                          {khasiat:"Mengandung madu & propolis"}]},
                      {nama: 'Sabun Madu Cair', harga:'2 btl @ 100 gr Rp 110.000',img:smc, id:'5',
                      opsi1:{ukuran:"2 botol", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"2 btl @ 100 gr Rp 110.000", 
                            listKhasiat:[{khasiat:"Menjaga kelebaban dan kelembutan kulit"},
                                          {khasiat:"Membersihkan kulit"},
                                          {khasiat:"Mengencangkan kulit"},
                                          {khasiat:"Mengurangi noda flek hitam dan jerawat"}]},
                      {nama: 'Shampoo Madu', harga:'2 btl @ 100 gr Rp 110.000',img:sm, id:'6',
                      opsi1:{ukuran:"2 botol", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"2 btl @ 100 gr Rp 110.000", 
                            listKhasiat:[{khasiat:"Mengurangi ketombe"},
                                          {khasiat:"Menguatkan akar rambut"},
                                          {khasiat:"Mencegah kerontokan rambut"},
                                          {khasiat:"Cocok untuk semua jenis kulit"}]},
                      {nama: 'Masker Madu', harga:'2 btl @ 70 gr Rp 110.000',img:mm, id:'7',
                      opsi1:{ukuran:"2 botol", harga:110000},
                            opsi2:{ukuran:"x", harga:0},
                            harga_full:"2 btl @ 70 gr Rp 110.000", 
                            listKhasiat:[{khasiat:"Mampu memberi nutrisi pada kulit"},
                                          {khasiat:"Meremajakan dan menghaluskan kulit"},
                                          {khasiat:"Membersihkan dan merawat kulit wajah"},
                                          {khasiat:"Mengangkat sel kulit mati"},
                                          {khasiat:"Mencerahkan kulit"}]}]);
        break;
      default:
        setInfoProduk([{nama: 'Madu Kelengkeng', harga:'400 gr Rp 110.000', img: kelengkeng, id:'1',
                            opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                          harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                      {khasiat:"Menguatkan fungsi otak"},
                                      {khasiat:"Memperlancar urin"},
                                      {khasiat:"Mengobati luka bakar"},
                                      {khasiat:"Mengobati sakit pinggang"},
                                      {khasiat:"Memperlancar penyembuhan pasca operasi"},
                                      {khasiat:"Bisa untuk masker wajah"}]},
                      {nama: 'Madu Durian', harga:'400 gr Rp 100.000',img: durian, id:'2',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Membuat enak tidur"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Mengobati luka bakar"},
                                        {khasiat:"Baik untuk masker wajah"}]},
                      {nama: 'Madu Kopi', harga:'400 gr Rp 100.000',img: kopi, id:'3',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Membuat enak tidur"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Baik untuk masker wajah"},
                                        {khasiat:"Mengobati luka bakar"}]},
                      {nama: 'Madu Kaliandra', harga:'400 gr Rp 100.000',img: kaliandra, id:'4',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Memperlancar pencernaan"},
                                        {khasiat:"Membantu menormalkan tekanan darah"},
                                        {khasiat:"Mengobati luka bakar"},
                                        {khasiat:"Mengurangi rasa mual"},
                                        {khasiat:"Menyuburkan kandungan"},
                                        {khasiat:"Mengobati reumatik"}]},
                      {nama: 'Madu Mangga', harga:'400 gr Rp 100.000',img: mangga, id:'5',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Mengurangi rasa mual"},
                                        {khasiat:"Memperkuat kandungan ibu hamil"},
                                        {khasiat:"Memperkuat fungsi otak"},
                                        {khasiat:"Mengobati luka bakar"},
                                        {khasiat:"Mengobati sariawan dan panas dalam"},
                                        {khasiat:"Menambah nutrisi tubuh"}]},
                      {nama: 'Madu Rambutan', harga:'400 gr Rp 100.000',img: rambutan, id:'6',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Memperlancar urin"},
                                        {khasiat:"Menguatkan fungsi ginjal"},
                                        {khasiat:"Mengobati luka bakar"},
                                        {khasiat:"Mengobati sakit maag"},
                                        {khasiat:"Mengobati batuk asma"},
                                        {khasiat:"Mengobati sakit pinggang"},
                                        {khasiat:"Sebagai penambah nutrisi tubuh"}]},
                      {nama: 'Madu Mahoni', harga:'400 gr Rp 100.000',img: mahoni, id:'7',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Mengobati sakit malaria"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Mengobati asam urat"},
                                        {khasiat:"Mengurangi rasa mual"},
                                        {khasiat:"Rasanya sedikit pahit manis"}]},
                      {nama: 'Madu Sonokeling', harga:'400 gr Rp 100.000',img: sonokeling, id:'8',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Memperlancar urin"},
                                        {khasiat:"Mengobati luka bakar"},
                                        {khasiat:"Mengobati sakit pinggang"},
                                        {khasiat:"Memperlancar penyembuhan pasca operasi"},
                                        {khasiat:"Bisa untuk masker wajah"}]},
                      {nama: 'Madu Kapuk Randu', harga:'400 gr Rp 100.000',img: kapuk_randu, id:'9',
                      opsi1:{ukuran:"400 gr", harga:100000},
                            opsi2:{ukuran:"1 kg", harga:210000},
                          harga_full:"400 gr Rp 100.000  //  1 kg Rp 210.000", 
                          listKhasiat:[{khasiat:"Meningkatkan nafsu makan"},
                                        {khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Mengobati sariawan dan luka bakar"},
                                        {khasiat:"Mengobati demam dan panas dalam"},
                                        {khasiat:"Baik untuk balita dan dewasa"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Memperlancar urin"},
                                        {khasiat:"Baik untuk masker wajah"},
                                        {khasiat:"Sebagai suplemen makanan"}]},
                      {nama: 'Madu Multiflora', harga:'400 gr Rp 90.000',img: multiflora, id:'10',
                      opsi1:{ukuran:"400 gr", harga:90000},
                            opsi2:{ukuran:"1 kg", harga:195000},
                          harga_full:"400 gr Rp 90.000  //  1 kg Rp 195.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Mengobati demam dan panas dalam"},
                                        {khasiat:"Mengobati sariawan dan luka bakar"},
                                        {khasiat:"Menguatkan fungsi otak"},
                                        {khasiat:"Baik untuk balita dan dewasa"},
                                        {khasiat:"Baik untuk masker wajah"}]},
                      {nama: 'Madu organik', harga:'400 gr Rp 110.000',img: organik, id:'11',
                      opsi1:{ukuran:"400 gr", harga:110000},
                            opsi2:{ukuran:"1 kg", harga:245000},
                          harga_full:"400 gr Rp 110.000  //  1 kg Rp 245.000", 
                          listKhasiat:[{khasiat:"Meningkatkan daya tahan tubuh"},
                                        {khasiat:"Mengobati sariawan dan luka bakar"},
                                        {khasiat:"Mengobati demam dan panas dalam"},
                                        {khasiat:"Baik untuk balita dan dewasa"},
                                        {khasiat:"Baik untuk masker wajah"}]},
                      {nama: 'Madu Hutan Super Kental', harga:'400 gr Rp 135.000',img: hutan, id:'12',
                      opsi1:{ukuran:"400 gr", harga:135000},
                            opsi2:{ukuran:"1,5 kg", harga:420000},
                          harga_full:"400 gr Rp 135.000 // 1,5 kg Rp 420.000", 
                          listKhasiat:[{khasiat:"Madu yang dipanen dari pedalaman Sumatera yang jauh dari polusi udara, sehingga kemurniannya tetap terjaga. Madu ini telah mengalami penurunan kadar air sehingga memenuhi standar internasional, tanpa mengurangi kandungan dan manfaatnya."}]},
                      {nama: 'Madu Ternak Super Kental', harga:'400 gr Rp 135.000',img: ternak, id:'13',
                      opsi1:{ukuran:"400 gr", harga:135000},
                            opsi2:{ukuran:"1 kg", harga:420000},
                          harga_full:" 400 gr Rp 135.000 // 1,5 kg Rp 420.000", 
                          listKhasiat:[{khasiat:"Madu ternak di bawah pengawasan yang ketat, sehingga keasliannya tetap terjaga. Kekentalannya memenuhi standar internasional"}]},
                      {nama: 'Madu Trigona', harga:'400 gr Rp 185.000',img: trigona, id:'14',
                      opsi1:{ukuran:"400 gr", harga:185000},
                            opsi2:{ukuran:"1 kg", harga:360000},
                          harga_full:"1 kg Rp 360.000 // 400 gr Rp 185.000", 
                          listKhasiat:[{khasiat:"Madu trigona ini dikenal juga dengan sebutan madu klanceng"},
                                        {khasiat:"Bagus untuk mengobati sariawan"},
                                        {khasiat:"Cocok untuk mengatasi gangguan lambung"},
                                        {khasiat:"Meningkatkan stamina tubuh"},
                                        {khasiat:"Sangat baik untuk mengatasi masuk angin"}]}]);
    }
  }, [state]);
    return (
        <>
        <Content>
          <Navbar />
            <div className="Header">
              <Row className="justify-content-md-center">
                <Col>
                  <h1 className="TittleHome">Aneka Madu</h1>
                </Col>
              </Row>
            </div>
            <Row className="sambutan justify-content-md-center">
              Selamat datang, berikut adalah produk yang kami sediakan:
            </Row>
            <Container className="container-list">
              <Row className="rowatas justify-content-md-center">
                <div className="tabs-filter">
                  <Tabs
                    className="tabkum"
                    id="controlled-tab"
                    activeState={state}
                    onSelect={(k) => setState(k)}
                  >
                    <Tab eventKey="Madu Murni" title="Madu Murni" />
                    <Tab eventKey="Madu Formula" title="Madu Formula" />
                    <Tab className="upcoming" eventKey="Madu Kosmetik" title="Madu Kosmetik" />
                  </Tabs>
                </div>
              </Row>
              <Row className="justify-content-md-center">
          {infoProduk.map((post) => (
            <CardsMadu
              key={post.id}
              gambar={post.img}
              merek={post.nama}
              harga={post.harga}
              fullinfo = {post}
              handleHargaChange = {handleHargaChange}
              handleProdukDibeliChange = {handleProdukDibeliChange}
            />
          ))}
        </Row>
            </Container>
            <Container className="listDanHarga" id="listHarga">
            <Row className="justify-content-md-center">
            <img src={cart} alt="cart" style={{ width: '10rem', height: '10rem',}} />
            </Row>
            <Col className="listBelanja justify-content-md-center">
                <div style={{marginLeft:'5vw'}}>Berikut adalah barang belanjaan kamu:</div>
                {''}
                <Row style={{marginLeft:'70px'}}>
                {produkDibeli.map((post) =>(
                    <Card style={{ width: '15rem', marginTop:'20px', marginLeft:'20px',}}>
                      <Card.Body className="isiBelanjaan">{post.nama}</Card.Body>
                    </Card>
                )
                )}
                </Row>

            </Col>

            <Col className="hargaBelanja justify-content-md-center">
              <div style={{marginLeft:'5vw'}}>Total harga belanjaan kamu adalah </div>
              <p className="totalHarga">Rp {harga}</p>
            </Col>

            <Row className="tombol justify-content-md-center">
              <Button className style={{marginLeft:'5vw', fontFamily: "'Open Sans', sans-serif"}} variant="success" onClick={() => setModalShow(true)}>Check Out Belanjaan</Button>
                <Button className style={{marginLeft:'5vw', fontFamily: "'Open Sans', sans-serif"}} variant="danger" onClick={resetData}>Kosongkan Keranjang</Button>
            </Row>
            </Container>

            {modalShow === true
          && (
    <ModalCheckout
          show={modalShow}
          onHide={() => setModalShow(false)}
          listBelanja={produkDibeli}
        />)}
            <Col className="footer">
              <div className="txtFooter">Untuk info lebih lanjut, Hubungi nomor berikut ini via whatsapp:</div>
              
              <Row className="nomor justify-content-md-center">
              <img src={wa} alt="gambar" style={{ width: '2rem', height: '2rem', }} />
              <div style={{ marginLeft:"5px"}}>085697977212</div>
              </Row>
            </Col>
          </Content>
        </>
      );
};

export default Home;
