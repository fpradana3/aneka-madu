import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { history } from './history';
import Home from './Home';
import "../index.css"

const Routes = () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={Home} />
    </Switch>
  </Router>
);

export default Routes;
